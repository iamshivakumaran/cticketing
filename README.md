# Issue Tracking System

## Introduction
**CTicketing** is a lightweight issue tracking system.

## Issue Format
The following elements can be maintained with TrackDown:
- **ID**
- **Title**
- **Status**
- **Commits**
- *Target Version* (optional)
- *Severity* (optional)
- *Affected Versions* (optional)
- **Description**
- **Comments**